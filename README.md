# Python cheatsheat

Little personal python cheatsheat  

### python
#### Объединение несколько одномерных списков в один:
```python
a=[1,2]
b=[3,4,5]
sum([a, b], []) == [1, 2, 3, 4, 5]
```
#### Обмен значениями, если количество принимаемых значений не известно 
###### В обычном случае:
```python
a, b=[1,2,3]
ValueError: too many values to unpack (expected 2)
```
###### Исправляется выбором элемента, который примет избыток: 
```python
a, *b = [1,2,3]
a, b == (1, [2, 3])
```

#### Удаление повтворяющихся элементов в списке:
```python
a=[1, 1, 2, 3, 2]
list(set(a)) == [1, 2, 3]
```
###### При этом порядок элементов **ТЕРЯЕТСЯ**
###### Сохранить его возможно используя OrderedDict
```python
a=[2, 1, 2, 3, 2]
from collections import OrderedDict
list(OrderedDict.fromkeys(items).keys()) == [2, 1, 3]
```
#### Тернарный оператор работает и при вызове функций
######
```python
(foo if a else bar)(arg)
```
#### Присвоение первого непустого значения
```python
a = None or False or 3d
a == 3
```
#### Значение по умолчанию для несуществующего элемента словаря 
###### Обычно
```python
d={"a":1,"b":2}
d["c"]
KeyError: "c"
```
###### Решается методом get
```python
d.get("c") == None
default = 0
d.get("c",default) == 0
```
#### Аргументы функции Print
###### Список аругментов
>**sep** - разделительные символы между элементами
>**end** - заключительные символы вывода

#### Сортировка словаря по значениями
######
```python
d={"c":1,"a"0}
sorted(d,key=d.get,reverse=False) == ["a","c"]
```
#### Офигенное создание словарей через аргументы функции
######
```python
{"one": 1, "two": 2, "three": 3} == dict(one=1, two=2, three=3)
```
#### Форматирование строк в python3.6+
```python
index="blah-blah"
f"{index} is index" 
```
> [Гайдик](https://shultais.education/blog/python-f-strings)
#### Кое-что про печать списков
######
```python
print(a)
[0, 1, 2, 3, 4]
```
А можно так:
```python
print(*a)
0 1 2 3 4
```

#### Кортежики
######
```python
article = "python", 2018, "LinearLeopard"            # объявление кортежа
theme, year, author = "python", 2018, "LinearLeopard"# распаковка кортежа
theme, year, _ = "python", 2018, "LinearLeopard"     # слева и справа должно
                                                     # находиться одинакове число
                                                     # переменных, можно подчеркнуть,
                                                     # что вам какая-то не нужно,
                                                     # обозначив её через
                                                     #  подчёркивание
theme, _, _ = "python", 2018, "LinearLeopard"        # имена могут повторяться
theme, * author = "python", 2018, "LinearLeopard"    # можно объявить жадный
                                                     # параметр, который съест
                                                     # все неподходящие,
                                                     # разумеется, допустим
                                                     # только один
                                                     # жадный оператор
```
#### Декоратор для подсчета времени исполннеия функции
######
```python
def benchmark(func): 
    def wrapper(*args, **kwargs):
        start = time.time()
        return_value = func(*args, **kwargs)
        end = time.time()
        t=end-start
        return return_value, t
    return wrapper
```

#### Шаблон для добавленния новых дефолтных обработчиков json.dumps
######
```python
from functools import singledispatch
from datetime import datetime 
import json
@singledispatch
def handler(arg):
   return arg

@handler.register(datetime)
def handler_dt(arg):
    return arg.isoformat()

json.dumps([2, datetime.now()], default=handler)    
```